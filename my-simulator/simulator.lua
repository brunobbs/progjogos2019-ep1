local Character = require("character")
local Weapon = require("weapon")

local SIMULATOR = {}

function SIMULATOR.run(scenario_input)
  -- Creates weapons
  local weapons = SIMULATOR.create_weapons(scenario_input)
  -- Creates Units
  local units = SIMULATOR.create_units(scenario_input, weapons)
  -- Apply seed
  math.randomseed(scenario_input.seed)
  -- Iterate the attack sequence
  units = SIMULATOR.iterate_attacks(scenario_input, units)

  return units
end

function SIMULATOR.create_weapons(scenario_input)
  local weapons = {}
  for name, weapon in pairs(scenario_input.weapons) do
    weapons[name] = Weapon.new(weapon, name)
  end
  return weapons
end

function SIMULATOR.create_units(scenario_input, weapons)
  local units = {}
  for name, unit in pairs(scenario_input.units) do
    units[name] =  Character.new(unit, name, weapons[unit.weapon])
  end
  return units
end

function SIMULATOR.iterate_attacks(scenario_input, units)
  for i, v in ipairs(scenario_input.fights) do
    print(i, v)
  end
  --TODO:all fighting mechanics
  return units
end

return SIMULATOR
