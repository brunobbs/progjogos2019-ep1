local Weapon = {}

local triangle_bonuses = {
    sword = {
        axe = 1,
        lance = -1
    },
    axe = {
        sword = -1,
        lance = 1
    },
    lance = {
        sword = 1,
        axe = -1
    },
    wind = {
        thunder = 1,
        fire = -1
    },
    thunder = {
        wind = -1,
        fire = 1
    },
    fire = {
        wind = 1,
        thunder = -1
    }
}

Weapon.prototype = {
    name = "Weapon Name",
    mt = 0,
    hit = 0,
    crt = 0,
    wt = 0,
    kind = false,
    eff = false
}

Weapon.meta = {}

Weapon.meta.__index = Weapon.prototype

function Weapon.new(new_weapon, name)
    setmetatable(new_weapon, Weapon.meta)
    if name then
        new_weapon.name = name
    end
    return new_weapon
end

function Weapon:triangle_bonus(def_weapon)
    return self.kind and def_weapon.kind and
        triangle_bonuses[self.kind][def_weapon.kind]
end

function Weapon:dmg_type()
    local phys_kinds = {sword = 1, axe = 1, lance = 1}
    local mag_kinds = {wind = 1, thunder = 1, fire = 1}
    if phys_kinds[self.kind] then
        return "phys"
    end
    if mag_kinds[self.kind] then
        return "mag"
    end
    error("Weapon damage type could not be specified")
    return nil
end

return Weapon
