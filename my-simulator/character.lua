local Character = {}

Character.prototype = {
    name = "Character Name",
    hp = 0,
    str = 0,
    mag = 0,
    skl = 0,
    spd = 0,
    lck = 0,
    def = 0,
    res = 0,
    weapon = false,
    trait = false
}

Character.meta = {}

Character.meta.__index = Character.prototype

function Character.new(new_char, name, weapon)
    setmetatable(new_char, Character.meta)
    if name then
        new_char.name = name
    end
    if weapon then
        new_char.weapon = weapon
    end

    return new_char
end

function Character:attack(def_char)
    if self.hp > 0 then
        -- check if will atk
        if self:will_attack(def_char) then
            -- check if crit
            if math.random(0, 100) < self:crit_chance(def_char) then
                def_char:take_damage(self:calc_damage(def_char, true))
            else
                def_char:take_damage(self:calc_damage(def_char, false))
            end
        end

        -- check if atk twice
        if self:get_atk_spd() - def_char:get_atk_spd() >= 4 then
            -- return true if attack twice
            return true
        end
    end
end

function Character:will_attack(def_char)
    local rand = (math.random(0, 100) + math.random(0, 100)) / 2
    if rand < self:hit_chance(def_char) then
        return true
    end
end

function Character:get_atk_spd()
    return self.spd - math.max(0, self.weapon.wt - self.str)
end

function Character:hit_chance(def_char)
    local accuracy =
        self.weapon.hit + self.skl * 2 + self.lck +
        self.weapon:triangle_bonus(def_char.weapon) * 10
    local avoid = (def_char:get_atk_spd() * 2) + def_char.lck
    return math.max(0, math.min(100, accuracy - avoid))
end

function Character:crit_chance(def_char)
    local crit_rate = self.weapon.crt + (self.skl / 2)
    local dodge = def_char.lck
    return math.max(0, math.min(100, crit_rate - dodge))
end

function Character:calc_damage(def_char, is_crit)
    local weapon_eff = 1
    local crit_val = 1
    if self.weapon.eff == def_char.trait then
        weapon_eff = 2
    end
    if is_crit then
        crit_val = 3
    end
    if self.weapon:dmg_type() == "phys" then
        return self:calc_phys_dmg(def_char, crit_val, weapon_eff)
    end
    if self.weapon:dmg_type() == "mag" then
        return self:calc_mag_dmg(def_char, crit_val, weapon_eff)
    end
end

function Character:calc_mag_dmg(def_char, crit, trait)
    local mag_pow =
        self.mag +
        (self.weapon.mt + self.weapon:triangle_bonus(def_char.weapon)) * trait
    return (mag_pow - def_char.res) * crit
end

function Character:calc_phys_dmg(def_char, crit, trait)
    local phys_pow =
        self.str +
        (self.weapon.mt + self.weapon:triangle_bonus(def_char.weapon)) * trait
    return (phys_pow - def_char.def) * crit
end

function Character:take_damage(amount)
    self.hp = self.hp - amount
end

return Character
